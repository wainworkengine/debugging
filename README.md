# Module of WainWorkEngine

No additional dependencies

## Getting started

Add this to your Packages\manifest.json
```
"com.wainwork.engine.debugging": "https://gitlab.com/wainworkengine/debugging.git#1.0.0",
```


### LoggedStopwatch
Easy way to calculate spended time for something.
```
using (new LoggedStopwatch(stamp => Debug.Log($"Loading take : {stamp:g}")))
    yield return new WaitForSecondsRealtime(3f);
```


### FPSCounter
Show current FPS. Draw overlay.

```
FPSCounter.Show();
FPSCounter.Hide();
```